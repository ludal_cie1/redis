import json
from flask import Flask, render_template, request, redirect
from elasticsearch import Elasticsearch
from datetime import datetime

app = Flask(__name__, template_folder='templates')
es = Elasticsearch(['http://elasticsearch:9200'])
_indexname = 'test'

# Fonction pour ajouter des données initiales à Elasticsearch à partir d'un fichier JSON
def add_initial_data_from_file(file_path='data.json'):

    with open(file_path, 'r') as file:
        initial_data = json.load(file)

    for data in initial_data:
        es.index(index=_indexname, doc_type='_doc', id=data['station'], body=data)

add_initial_data_from_file()

@app.route('/', methods=['GET'])
def index():
    # Utilisez les opérations Elasticsearch pour récupérer les données
    index_name = 'test'
    response = es.search(index=index_name)
    hits = response['hits']['hits']

    all_data = [hit['_source'] for hit in hits]

    return render_template('index.html', all_data=all_data)

@app.route('/add', methods=['GET', 'POST'])
def add_data():
    current_date = datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z")

    if request.method == 'POST':
        # Récupération des données
        new_station_name = request.form.get('new_station_name')
        new_status = request.form.get('new_status')
        new_capacity = request.form.get('new_capacity')
        new_nbbornettesLibre = request.form.get('new_nbbornettesLibre')
        # ... (ajoutez les autres champs ici)

        # Ajout dans Elasticsearch
        response = es.index(index='vlib', body={
            'station': new_station_name,
            'status': new_status,
            'capacity': new_capacity,
            'nbbornettesLibre': new_nbbornettesLibre,
            # ... (ajoutez les autres champs ici)
            'actualisation': current_date
        })

        return redirect('/')

    return render_template('ajout.html', current_date=current_date)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
