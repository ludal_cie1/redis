# Utilise une image de base Python
FROM python:latest
# Installation des paquets nécessaires
RUN apt-get update

# Crée un répertoire de travail
WORKDIR /app

# Copie les fichiers nécessaires dans le conteneur
COPY app.py .
COPY data.json . 
COPY ./templates . 

# Installe les dépendances
RUN pip install flask
RUN pip install elasticsearch==6.3.1 

# Expose le port 80 (ou le port que tu utilises)
EXPOSE 80

# Commande pour exécuter l'application
CMD ["python", "app.py"]
